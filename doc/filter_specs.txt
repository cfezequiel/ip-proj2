Parameter File Filter Specifications

Filters:
    histogram       Render histogram image on top of ROI of a given image file
    histoequalize   Perform histogram equalization on a ROI of a given image
                    file

Common Filter Parameters:
    colorspace      Specify the colorspace where histogram is to be performed
                    Values:
                    0 - Greyscale
                    1 - RGB
                    2 - HSI
    channels        Specify the channel or channels where the filter should be
                    applied. Multiple channels are specified by ORing the
                    channel values. This parameter is only defined 'colorspace'
                    is RGB or HSI
                    Values:
                    0x1 - Channel 0 (R or H)
                    0x2 - Channel 1 (G or S)
                    0x4 - Channel 2 (B or I)

Examples:

histogram 0         Render histogram image of Grey channel for grey image
histogram 1 3       Render histograms of Red and Green channels of an RGB image
histoequalize 2 7   Perform histogram equalization of Hue, Saturation and
                    Intensity channels of a color image


    
