CAP5400 (Fall 2012) Assignment 2 - Image and Video Histogram Equalization

Author: Carlos Ezequiel

Directory structure
-------------------

This software is architectured as follows

/iptools -This folder hosts the files that are compiled into a static library. 
	image - This folder hosts the files that define an image.
	utility - this folder hosts the files that students store their implemented algorithms.
	video - This folder hosts the files that define an video.
	
/lib- This folder hosts the static libraries associated with this software.

/project - This folder hosts the files that will be compiled into executables.
	/bin - This folder hosts the binary executables created in the project directory.
    /tests - This folder contains test files
        /input - Source image and video files
        /output - Processed image and video files are dumped here by the test
                  script
        /conf - Contains test parameter files
        gen_test.py - Auto-generates test scripts and parameter files

/doc - Contains documentation about this project
	

Compiling the program 
----------------------

To compile this software enter the /project directory and run `make`.


Running the program 
----------------------

The program may be run from /project/bin directory.
Program syntax:

$ ./iptool [-s -g] SRC TGT PARAM

where:
    SRC - source image/video file
    TGT - output image/video file
    PARAM - Parameter file

Options:
    -g      Force greyscale frame rendering (for video only)
    -s      Use sequence-of-interest (SOI) processing. This is mainly used
            for obtaining the histogram using all the frames within the SOI
            of the video

The following image file/s are supported: .ppm, .pgm
The following video file/s are supported: .m1v, .mpeg

If SRC is a video file, TGT is an image file, and -s is given, the program
will generate a histogram image for each ROI across each corresponding
SOI of the video, as given in the parameter file. The image will be taken
from the first frame of the first SOI specified in the parameter file.

Running the program without any arguments prints the syntax for the program

The parameter file format specification is located in /doc.


Running the tests
-----------------

To run the tests, compile the program first.
Then, generate the tests. Go to /projects/tests and execute

$ make

Or, generate the tests using the Python script directly:

$ ./gen_test.py

This should result in two test scripts:

run_img.sh      Runs image tests
run_vid.sh      Runs video tests

Also, the parameter file for each test will be generated in tests/conf
directory.

Run the tests in a bash shell:

$ ./run_img.sh
$ ./run_vid.sh

The scripts will print out each test being performed, and after
completion, the time it took to process each test.

NOTE: The video tests will take a considerably long time to complete,
especially in the GRAD server.


Report
------
## Introduction ##
The goal of this assignment is to investigate contrast en- hancement of image and video through histogram equalization (HE). Histogram equalization is a method that involves redis- tribution of the pixel intensities of an image’s histogram over a wider range of values. The technique useful in enhancing images that are under-exposed, or have backgrounds and fore- grounds that are either both dark or both light. One application is in enhancing medical images, such as those taken from a microscope or X-ray.
The assignment will investigate histogram equalization ap- plied to still images in the Greyscale, RGB and HSI color spaces. The assignment will also apply the same histogram equalization algorithm to video.

## Conclusion ##
The tests were able to show the effects of histogram equalization (HE) performed on various color channels (Grey, RGB and HSI) of images and video. Greyscale HE appeared to have the most even distribution (ideally, the histogram graph should be horizontal line). RGB HE image tests did not reveal a very noticeable difference in image quality as a result of enhancement. HSI HE tests showed the unique qualities of manipulating each channel. HE done on the intensity (I) channel appeared to have the best contrast enhancement for color images. The effects of HE was most noticeable when all three channels were enhanced for each pixel.
Video HE tests have similar results to image HE tests. However, the video tests took significantly longer time to complete, due to the amount of pixel data involved. Performing HE on a per-frame basis in video can lead to an inconsistent brightness across frames. Performing HE on a per-sequence basis can lead to consistent contrast enhancement, but may not always lead to an even distribution, as shown in one test.
The parameters that affect performance are the colorspace used, the number of channels, and the number of images or frames in the case of video. HSI HE took longer time to complete than RGB HE due to colorspace conversion. Increasing the channels and frames where HE was performed resulted in longer processing time. Per-frame and per-SOI video HE processing time differed by only a small margin.