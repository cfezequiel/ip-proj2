#ifndef VIDEO_H
#define VIDEO_H

#include <vector>
#include <string>
#include "../image/image.h"

class frame: public image
{
    public:
        int seqNum;
        string path;

        frame() : image() {}
        frame(string path, int seqNum);
        frame(const frame &f) {}
        void assign(string path, int seqNum);
        void save();
        void read();
};

class video 
{
	private: 
        string _frameFileFormat;
        string _frameDir;
        vector<frame> _frames;

	public: 

		video();
        video(string filename, COLORSPACE cs);
		~video();

        void load(string filename, COLORSPACE cs);
        void save(string filename);
        void close();
        int getFrameCount();
        frame * getFrame(int i);
        void setFrame(int i, image img);
};

#endif

