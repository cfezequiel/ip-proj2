#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <assert.h>
#include <vector>
#include "../common.h"
#include "../ipparams/ipparams.h"
#include "../image/image.h"
#include "../video/video.h"

enum RGBCOLOR
{
    RGBCOLOR_GREY    = 0x999999,
    RGBCOLOR_RED     = 0xff0000,
    RGBCOLOR_GREEN   = 0x00ff00,
    RGBCOLOR_BLUE    = 0x0000ff,
    RGBCOLOR_YELLOW  = 0xffff00,
    RGBCOLOR_CYAN    = 0x00ffff,
    RGBCOLOR_MAGENTA = 0xff00ff
};


class Histogram
{
    private:
        vector<int> _h;
        int _hmax;
        int _hsize;
        
    public:
        Histogram(image &src, ROI roi, int channel);
        Histogram(ImageHSI &src, ROI roi, int channel);
        Histogram(video &src, ROI roi, int channel, SOI soi);
        Histogram(video &vid, ROI roi, HSICHANNEL channel, SOI soi);
        void overlayGraph(image &tgt, ROI roi, int rgbcolor);
        void equalize(image &img, ROI roi, int channel);
        void equalize(ImageHSI &img, ROI roi, int channel);
        void equalize(video &vid, ROI roi, int channel, SOI soi);
        void equalize(video &vid, ROI roi, HSICHANNEL channel, SOI soi);
        vector<int> pixGraph();
        ~Histogram();
};

#endif //HISTOGRAM_H

