#include <assert.h>
#include <vector>
#include <cmath>
#include <stdlib.h>
#include <strings.h>
#include <sstream>
#include <string.h>
#include <time.h>
#include "../common.h"
#include "utility.h"
#include "histogram.h"
using namespace std;

/**
 * Calculates the Euclidean distance between two vectors
 */
int eucdist(int *p1, int *p2, int dim)
{
    assert(dim > 0);
    assert(p1 != NULL);
    assert(p2 != NULL);

    int d = 0;
    for (int i = 0; i < dim; i++)
    {
        d += (p1[i] - p2[i]) * (p1[i] - p2[i]);
    }
    d = (int) ceil(sqrt(d));

    return d;
}


/**
 * Compute the window mean
 */
int computeWindowMean(image &img, int hs, int vs, int i, int j)
{
    int windowMean;
    int p;
    int q;
    int n;
    int imgWidth = img.getNumberOfColumns();
    int imgHeight = img.getNumberOfRows();

    // Assume odd window size (center-reference)
    assert(hs > 0);
    assert(vs > 0);

    // Compute window mean
    windowMean = 0;
    for (int m = -hs; m <= hs; m++)
    {
        for (int n = -vs; n <= vs; n++)
        {
            p = i + m;
            q = j + n;
            if (p < 0 || p >= imgWidth || q < 0 || q >= imgHeight)
            {
                continue;
            }
            windowMean += img.getPixel(p, q);
        }
    }
    n = (hs * 2 + 1) * (vs * 2 + 1);
    windowMean = (windowMean + n) / n;

    return windowMean;
}

/*-----------------------------------------------------------------------**/
//
// Sets all the pixels within the ROI to 0 value (i.e. black)
void utility::clear(image &tgt, ROI roi)
{
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            tgt.setPixel(i, j, RED, 0);
            tgt.setPixel(i, j, GREEN, 0);
            tgt.setPixel(i, j, BLUE, 0);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::addGrey(image &src, image &tgt, IPParam p)
{
    int pixValue;

    // Run algorithm
    int weight = p.filter_params[0];
    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            pixValue = src.getPixel(i, j) + weight;
            if (pixValue > 255)
            {
                pixValue = 255;
            }
            else if (pixValue < 0)
            {
                pixValue = 0;
            }
            else 
            {
                pixValue = src.getPixel(i, j);
            }
            tgt.setPixel(i, j, pixValue);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::greyThresh(image &src, image &tgt, IPParam p)
{
    int pixValue;
    int threshold;

    // Run algorithm on ROI
    threshold = p.filter_params[0];
    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            pixValue = src.getPixel(i, j);
            if (pixValue >= threshold)
            {
                pixValue = 255;
            }
            else
            {
                pixValue = 0;
            }
            tgt.setPixel(i, j, pixValue);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::colorThresh(image &src, image &tgt, IPParam p)
{
    int threshold;
    int color[NCHANNELS];
    int pixel[NCHANNELS];

    // Get threshold value for ROI
    threshold = p.filter_params[0];

    // Get color comparison value for ROI
    color[0] = p.filter_params[1];
    color[1] = p.filter_params[2];
    color[2] = p.filter_params[3];

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            pixel[0] = src.getPixel(i, j, RED);
            pixel[1] = src.getPixel(i, j, GREEN);
            pixel[2] = src.getPixel(i, j, BLUE);

            if (eucdist(pixel, color, 3) <= threshold)
            {
                tgt.setPixel(i, j, RED, 255);
                tgt.setPixel(i, j, GREEN, 255);
                tgt.setPixel(i, j, BLUE, 255);
            }
            else
            {
                tgt.setPixel(i, j, RED, 0);
                tgt.setPixel(i, j, GREEN, 0);
                tgt.setPixel(i, j, BLUE, 0);
            }
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::adaptiveThresh(image &src, image &tgt, IPParam p)
{
    int threshold;
    int windowSize;
    int pixValue;
    int weight;
    int windowMean;

    // Get window size for ROI
    windowSize = p.filter_params[0];

    // Get weight value for ROI
    weight = p.filter_params[1];

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            // Compute the window mean
            windowMean = computeWindowMean(src, windowSize, windowSize, i, j);

            // Set threshold
            threshold = windowMean + weight;

            // Apply threshold
            pixValue = src.getPixel(i, j);
            if (pixValue >= threshold)
            {
                pixValue = 255;
            }
            else
            {
                pixValue = 0;
            }
            tgt.setPixel(i, j, pixValue);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::smoothing2D(image &src, image &tgt, IPParam p)
{
    int windowSize;
    int windowMean;

    // Get window size for ROI
    windowSize = p.filter_params[0];

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            // Compute the window mean
            windowMean = computeWindowMean(src, windowSize, windowSize, i, j);
            tgt.setPixel(i, j, windowMean);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::smoothing1D(image &src, image &tgt, IPParam p)
{
    int windowSize;
    int windowMean;

    // Get window size for ROI
    windowSize = p.filter_params[0];

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            // Process horizontal window averaging
            windowMean = computeWindowMean(src, windowSize, 1, i, j);
            tgt.setPixel(i, j, windowMean);
        }
    }

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            // Process vertical window averaging
            windowMean = computeWindowMean(tgt, 1, windowSize, i, j);
            tgt.setPixel(i, j, windowMean);
        }
    }
}

#if 0 //TODO: refactor
/*-----------------------------------------------------------------------**/
void utility::scale(image &src, image &tgt, float value)
{
    int width = src.getNumberOfRows();
    int height = src.getNumberOfColumns();

    tgt.resize(width * value, height * value);

    // Scale down
    if (value < 1)
    {
        for (int i = 0; i < tgt.getNumberOfRows(); i++)
        {
            for (int j = 0; j < tgt.getNumberOfColumns(); j++)
            {
                int sum = 0;
                int ave;
                for (int ii = 0; ii < value; ii++)
                {
                    for (int jj = 0; jj < value; jj++)
                    {
                        sum += src.getPixel(i / value + ii, j / value + jj);
                        ave = sum / value;
                    }
                }
                tgt.setPixel(i, j, ave);
            }
        }
    }
    // Scale Up
    else
    {
        for (int i = 0; i<width; i++)
        {
            for (int j = 0; j<height; j++)
            {
                int pixel = src.getPixel(i, j); 
                for (int ii = 0; ii < value; ii++)
                {
                    for (int jj = 0; jj < value; jj++)
                    {
                        tgt.setPixel(i * value + ii, j * value + jj, pixel); 
                    }
                }
            }
        }
    } // end if-else
}

#endif

/*-----------------------------------------------------------------------**/
void utility::histogram(image &src, image &tgt, IPParam p)
{
    assert(p.filter == "histogram");

    int channel;
    ImageHSI *hsiImage;
    Histogram *h;

    // Clear the ROI
    utility::clear(tgt, p.roi);

    switch (p.filter_params[0])
    {
        case CS_GREY:
            // Generate histogram for grey channel
            h = new Histogram(src, p.roi, GREY);
            h->overlayGraph(tgt, p.roi, RGBCOLOR_GREY);
            delete h;
            break;

        case CS_RGB:
            channel = p.filter_params[1];
            if (channel & RED)
            {
                // Generate histogram for red channel
                h = new Histogram(src, p.roi, RED);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_RED);
                delete h;
            }
            if (channel & GREEN)
            {
                // Generate histogram for green channel
                h = new Histogram(src, p.roi, GREEN);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_GREEN);
                delete h;
            }
            if (channel & BLUE)
            {
                // Generate histogram for blue channel
                h = new Histogram(src, p.roi, BLUE);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_BLUE);
                delete h;
            }
            break;

        case CS_HSI:
            // Convert image to HSI color space
            hsiImage = new ImageHSI(src);

            channel = p.filter_params[1];
            if (channel & HUE)
            {
                // Generate histogram for hue channel
                h = new Histogram(*hsiImage, p.roi, HUE);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_YELLOW);
                delete h;
            }
            if (channel & SATURATION)
            {
                // Generate histogram for saturation channel
                h = new Histogram(*hsiImage, p.roi, SATURATION);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_CYAN);
                delete h;
            }
            if (channel & INTENSITY)
            {
                // Generate histogram for intensity channel
                h = new Histogram(*hsiImage, p.roi, INTENSITY);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_MAGENTA);
                delete h;
            }

            delete hsiImage;
            break;

        default:
            throw IPException("Unknown colorspace.");
    }
}

/*-----------------------------------------------------------------------**/
void utility::histogram(video &vid, image &img, IPParam p)
{
    assert(p.filter == "histogram");

    Histogram *h;
    int channel;
    frame *frm;

    // Set the output image
    if (img.isEmpty())
    {
        // Get first frame of first SOi of video
        frm = vid.getFrame(p.soi.beg);
        img.copyImage(*frm);
    }
    utility::clear(img, p.roi);

    switch (p.filter_params[0])
    {
        case CS_GREY:
            // Generate histogram for grey channel
            h = new Histogram(vid, p.roi, GREY, p.soi);
            h->overlayGraph(img, p.roi, RGBCOLOR_GREY);
            delete h;
            break;

        case CS_RGB:
            channel = p.filter_params[1];
            if (channel & RED)
            {
                // Generate histogram for red channel
                h = new Histogram(vid, p.roi, RED, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_RED);
                delete h;
            }
            if (channel & GREEN)
            {
                // Generate histogram for green channel
                h = new Histogram(vid, p.roi, GREEN, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_GREEN);
                delete h;
            }
            if (channel & BLUE)
            {
                // Generate histogram for blue channel
                h = new Histogram(vid, p.roi, BLUE, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_BLUE);
                delete h;
            }
            break;

        case CS_HSI:
            channel = p.filter_params[1];
            if (channel & HUE)
            {
                // Generate histogram for hue channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) HUE, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_YELLOW);
                delete h;
            }
            if (channel & SATURATION)
            {
                // Generate histogram for saturation channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) SATURATION, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_CYAN);
                delete h;
            }
            if (channel & INTENSITY)
            {
                // Generate histogram for intensity channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) INTENSITY, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_MAGENTA);
                delete h;
            }
            break;

        default:
            throw IPException("Unknown colorspace.");
    }
}
/*-----------------------------------------------------------------------**/
void utility::histoEqualize(image &src, image &tgt, IPParam p)
{
    assert(p.filter == "histoequalize");

    int channel;
    ImageHSI *hsiImage;
    Histogram *h;

    switch (p.filter_params[0])
    {
        case CS_GREY:
            // Generate histogram for grey channel
            h = new Histogram(src, p.roi, GREY);
            h->equalize(tgt, p.roi, GREY);
            delete h;
            break;

        case CS_RGB:
            channel = p.filter_params[1];
            if (channel & RED)
            {
                // Generate histogram for red channel
                h = new Histogram(src, p.roi, RED);
                h->equalize(tgt, p.roi, RED);
                delete h;
            }
            if (channel & GREEN)
            {
                // Generate histogram for green channel
                h = new Histogram(src, p.roi, GREEN);
                h->equalize(tgt, p.roi, GREEN);
                delete h;
            }
            if (channel & BLUE)
            {
                // Generate histogram for blue channel
                h = new Histogram(src, p.roi, BLUE);
                h->equalize(tgt, p.roi, BLUE);
                delete h;
            }
            break;

        case CS_HSI:
            // Convert image to HSI color space
            hsiImage = new ImageHSI(src);

            channel = p.filter_params[1];
            if (channel & HUE)
            {
                // Generate histogram for hue channel
                h = new Histogram(*hsiImage, p.roi, HUE);
                h->equalize(*hsiImage, p.roi, HUE);
                delete h;
            }
            if (channel & SATURATION)
            {
                // Generate histogram for saturation channel
                h = new Histogram(*hsiImage, p.roi, SATURATION);
                h->equalize(*hsiImage, p.roi, SATURATION);
                delete h;
            }
            if (channel & INTENSITY)
            {
                // Generate histogram for intensity channel
                h = new Histogram(*hsiImage, p.roi, INTENSITY);
                h->equalize(*hsiImage, p.roi, INTENSITY);
                delete h;
            }

            // Convert image back to RGB space
            hsiImage->toRGB(tgt, p.roi.x, p.roi.y, p.roi.sx, p.roi.sy);

            delete hsiImage;
            break;

        default:
            throw IPException("Unknown colorspace.");
    }
}


/*-----------------------------------------------------------------------**/
void utility::histoEqualize(video &vid, IPParam p)
{
    assert(p.filter == "histoequalize");

    Histogram *h;
    int channel;

    switch (p.filter_params[0])
    {
        case CS_GREY:
            // Generate histogram for grey channel
            h = new Histogram(vid, p.roi, GREY, p.soi);
            h->equalize(vid, p.roi, GREY, p.soi);
            delete h;
            break;

        case CS_RGB:
            channel = p.filter_params[1];
            if (channel & RED)
            {
                // Generate histogram for red channel
                h = new Histogram(vid, p.roi, RED, p.soi);
                h->equalize(vid, p.roi, RED, p.soi);
                delete h;
            }
            if (channel & GREEN)
            {
                // Generate histogram for green channel
                h = new Histogram(vid, p.roi, GREEN, p.soi);
                h->equalize(vid, p.roi, GREEN, p.soi);
                delete h;
            }
            if (channel & BLUE)
            {
                // Generate histogram for blue channel
                h = new Histogram(vid, p.roi, BLUE, p.soi);
                h->equalize(vid, p.roi, BLUE, p.soi);
                delete h;
            }
            break;

        case CS_HSI:
            channel = p.filter_params[1];
            if (channel & HUE)
            {
                // Generate histogram for hue channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) HUE, p.soi);
                h->equalize(vid, p.roi, (HSICHANNEL) HUE, p.soi);
                delete h;
            }
            if (channel & SATURATION)
            {
                // Generate histogram for saturation channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) SATURATION, p.soi);
                h->equalize(vid, p.roi, (HSICHANNEL) SATURATION, p.soi);
                delete h;
            }
            if (channel & INTENSITY)
            {
                // Generate histogram for intensity channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) INTENSITY, p.soi);
                h->equalize(vid, p.roi, (HSICHANNEL) INTENSITY, p.soi);
                delete h;
            }
            break;

        default:
            throw IPException("Unknown colorspace.");
    }
}

/*-----------------------------------------------------------------------**/
void utility::processImage(image &src, image &tgt, IPParam p)
{
    // Check if ROI is within image
    if (!p.roi.inImage(src))
    {
        throw IPException("ROI is not within the image.");
    }

    // Copy source image to target image if new
    if (tgt.isEmpty())
    {
        tgt.copyImage(src);
    }

    if (p.filter == "greyadd") 
    {
        utility::addGrey(src, tgt, p);
    }
    else if (p.filter == "greythresh") 
    {
        // do greyscale thresholding
        utility::greyThresh(src, tgt, p);
    }
#if 0
    else if (p.filter == "colorthresh") 
    {
        // do RGB-color thresholding
        utility::colorThresh(src, tgt, p);
        
    }
    else if (p.filter == "adaptivethresh")
    {
        // do adaptive thresholding
        utility::adaptiveThresh(src, tgt, p);
        
    }
    else if (p.filter == "smoothing2d")
    {
        // do 2D smoothing 
        utility::smoothing2D(src, tgt, p);
        
    }
    else if (p.filter == "smoothing1d")
    {
        // do 1D smoothing 
        utility::smoothing1D(src, tgt, p);
    }
    else if (p.filter == "scale") {

        // TODO: re-implement
        // do scaling
        utility::scale(src, tgt, (float) atof(argv[4]));

    }
#endif
    else if (p.filter == "histogram")
    {
        histogram(src, tgt, p);
    }
    else if (p.filter == "histoequalize")
    {
        histoEqualize(src, tgt, p);
    }
    else
    {
        stringstream m;
        m << "Unrecognized or disabled filter: " << p.filter << '.';
        throw IPException(m.str().c_str());
    }
}

void utility::processVideo(string src, string tgt, vector<IPParam> ps,
        COLORSPACE cs, VID_MODE mode)
{
    // Open video
    video vid(src, cs);
    
    vector<IPParam>::iterator ip;
    // For each frame, apply image processing based on given params
    if (mode == PER_FRAME)
    {
        frame *fsrc;
        for (int i = 0; i < vid.getFrameCount(); i++)
        {
            for (ip = ps.begin(); ip != ps.end(); ip++)
            {
                // Check if frame is within SOI
                fsrc = vid.getFrame(i);
                if (ip->soi.inSOI(*fsrc))
                { 
                    // Do image processing
                    image tgt;
                    processImage(*fsrc, tgt, *ip);
                    vid.setFrame(i, tgt);
                }
            }
        }
    }
    else if (mode == PER_SOI)
    {
        for (ip = ps.begin(); ip != ps.end(); ip++)
        {
            if (ip->filter == "histoequalize")
            {
                histoEqualize(vid, *ip);
            }
            else if (ip->filter == "histogram");
            {
                // do nothing?
            }
        }
    }
    else
    {
        throw IPException("Unknown video processing mode");
    }

    // Close video
    vid.save(tgt);
    vid.close();
}

void utility::processVideo(string src, image &tgt, vector<IPParam> ps,
        COLORSPACE cs, VID_MODE mode)
{
    assert(mode == PER_SOI);

    // Open video
    video vid(src, cs);
    
    vector<IPParam>::iterator ip;
    // For each frame, apply image processing based on given params
    for (ip = ps.begin(); ip != ps.end(); ip++)
    {
        if (ip->filter == "histogram")
        {
            histogram(vid, tgt, *ip);
        }
    }

    // Close video
    vid.close();
}
