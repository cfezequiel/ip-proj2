#ifndef UTILITY_H
#define UTILITY_H

#include "../ipparams/ipparams.h"
#include "../video/video.h"
#include "../image/image.h"
#include <sstream>

enum VID_MODE
{
    PER_FRAME = 0,
    PER_SOI = 1
};

class utility
{
	public:
		utility();
		virtual ~utility();
		static std::string intToString(int number);
        static void clear(image &tgt, ROI roi);
        static void addGrey(image &src, image &tgt, IPParam p);
        static void greyThresh(image &src, image &tgt, IPParam p);
        static void colorThresh(image &src, image &tgt, IPParam p);
        static void scale(image &src, image &tgt, float value);
        static void adaptiveThresh(image &src, image &tgt, IPParam p);
        static void smoothing2D(image &src, image &tgt, IPParam p);
        static void smoothing1D(image &src, image &tgt, IPParam p);
        static void histogram(image &src, image &tgt, IPParam p);
        static void histogram(video &vid, image &img, IPParam p);
        static void histoEqualize(image &src, image &tgt, IPParam p);
        static void histoEqualize(video &vid, IPParam p);
        static void processImage(image &src, image &tgt, IPParam p);
        static void processVideo(string src, string tgt, vector<IPParam> ps,
                COLORSPACE cs, VID_MODE mode);
        static void processVideo(string src, image &tgt, vector<IPParam> ps,
                COLORSPACE cs, VID_MODE mode);
};

#endif

