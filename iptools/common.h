#ifndef COMMON_H
#define COMMON_H
#include <stdexcept>

class IPException: public std::runtime_error
{
    public:
        IPException(const char *m) : std::runtime_error(m) {};
};

#endif //COMMON_H

