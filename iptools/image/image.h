#ifndef IMAGE_H
#define IMAGE_H
#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include <string>
#include <vector>

using namespace std;

#define PI        3.14159265358979f
#define G_RANGE   256
#define RGB_MAX   255
#define H_MAX     360
#define S_MAX     100
#define I_MAX     255
#define RGB_RANGE G_RANGE
#define NCHANNELS 3

enum RGBCHANNEL
{
    GREY  = 0x0,
    RED   = 0x1, 
    GREEN = 0x2, 
    BLUE  = 0x4
};

enum HSICHANNEL 
{
    HUE         = 0x1, 
    SATURATION  = 0x2, 
    INTENSITY   = 0x4
}; 

enum COLORSPACE
{
    CS_GREY = 0,
    CS_RGB  = 1,
    CS_HSI  = 2
};

struct imageData
{
   vector<int>  redChannel, greenChannel, blueChannel; 
   int numRows, numColumns; 
};

class ImageBase
{
    protected:
        int _numRows;
        int _numColumns; 

    public:
        void setPixel(int row, int col, int channel, int value);
        int getPixel(int row, int col, int channel);
        int getNumberOfRows() {return _numRows;} 
        int getNumberOfColumns() {return _numColumns;}
        vector<int>* getChannel(int channel);
};

class image 
{
    private:
       imageData data;
       int getint(FILE *fp);
       
    public:
        image();
        image(string file);
        image(image &img);
        image(int rows, int columns);
        ~image();

        void deleteImage();
        void copyImage(image &img);
        void resize (int numberOfRows, int numberOfColumns);
        void setNumberOfRows(int rows);
        void setNumberOfColumns(int columns);
        void setPixel(const int row, const int col, const int value);
        void setPixel(const int row, const int col, const int rgb, const int value);

        int getPixel(const int row, const int col);
        int getPixel(const int row, const int col, const int rgb);
        int getNumberOfRows();
        int getNumberOfColumns(); 

        vector<int>* getChannel(int rgb);
        bool setChannel(int rgb, vector<int> &channel);   

        bool save (char* file);
        bool save (const char* file);
        bool read (const char* file);

        bool isInbounds (const int row, const int col);
        bool isEmpty();
};

class ImageHSI : public ImageBase
{
    private:
        vector<int> _h;
        vector<int> _s;
        vector<int> _i;
        void _genHSI(image &img);

    public:
        ImageHSI(image &img);
        void resize(int nRows, int nColumns);
        int getPixel(int row, int col, int channel);
        void setPixel(int row, int col, int channel, int value);
        void toRGB(image &img);
        void toRGB(image &img, int x, int y, int sx, int sy);
        vector<int>* getChannel(int channel);
};

void rgb2hsi(int r, int g, int b, double *h, double *s, double *i);
void hsi2rgb(double h, double s, double i, int *r, int *g, int *b);

#endif

