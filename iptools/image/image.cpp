#include <cassert>
#include <cmath>
#include <string.h>
#include <float.h>
#include <limits.h>
#include "../common.h"
#include "../ipparams/ipparams.h"
#include "image.h"

image::image()
{
	data.numRows = data.numColumns = 0; 
	data.redChannel.clear();
	data.greenChannel.clear();
	data.blueChannel.clear();
}

/*-----------------------------------------------------------------------**/
image::image(string file)
{
	this->read(file.c_str());
}

/*-----------------------------------------------------------------------**/
image::image(image &img)
{
	this->copyImage(img);
}

/*-----------------------------------------------------------------------**/
image::image(int rows, int columns) 
{
	this->data.numRows = rows;
	this->data.numColumns = columns; 
	this->resize (rows, columns);
}

/*-----------------------------------------------------------------------**/
image::~image() 
{
	this->deleteImage();
}

/*-----------------------------------------------------------------------**/
bool image::isInbounds (int row, int col) 
{
	if ((row < 0) || (col < 0) || (col >= data.numColumns) || (row >= data.numRows))
		return false;
	else 
		return true;
}

/*-----------------------------------------------------------------------**/
void image::deleteImage() 
{
	this->data.numRows = this->data.numColumns = 0; 
	this->data.redChannel.clear();
	this->data.greenChannel.clear();
	this->data.blueChannel.clear();
}

/*-----------------------------------------------------------------------**/
void image::copyImage(image &img) 
{
	this->resize(img.getNumberOfRows() ,img.getNumberOfColumns());

    this->setChannel(RED, *img.getChannel(RED));
    this->setChannel(GREEN, *img.getChannel(GREEN));
    this->setChannel(BLUE, *img.getChannel(BLUE));
}


/*-----------------------------------------------------------------------**/
void image::resize (int rows, int columns)
{
	int length = rows*columns;
	data.numRows = rows;
	data.numColumns = columns;
	data.redChannel.clear();
	data.greenChannel.clear();
	data.blueChannel.clear();

	data.redChannel.resize(length); 
	data.greenChannel.resize(length);
	data.blueChannel.resize(length);

}

/*-----------------------------------------------------------------------**/
void image::setNumberOfRows (int rows)
{
	data.numRows = rows;
}

/*-----------------------------------------------------------------------**/
void image::setNumberOfColumns (int columns)
{
	data.numColumns = columns;
}

/*-------------------------------------------------------------------*/
void image::setPixel(const int row, const int col, const int value) 
{
	data.redChannel [row*data.numColumns+col] = value; 
}


/*-------------------------------------------------------------------*/
void image::setPixel(const int row, const int col, const int rgb, const int value)
{
	switch (rgb) 
	{
        case RED: case GREY: data.redChannel [row*data.numColumns+col] = value; break;
		case GREEN: data.greenChannel [row*data.numColumns+col] = value; break;
		case BLUE: data.blueChannel [row*data.numColumns+col] = value; break;
	}
}

/*-------------------------------------------------------------------*/
int image::getPixel(const int row, const int col) 
{ 
	return data.redChannel [row*data.numColumns+col]; 
}

/*-------------------------------------------------------------------*/
int image::getPixel(const int row, const int col, const int rgb) 
{
	int value;
	switch (rgb)
	{
        case RED: case GREY: value = data.redChannel [row*data.numColumns+col]; break;
		case GREEN: value = data.greenChannel [row*data.numColumns+col]; break;
		case BLUE: value = data.blueChannel [row*data.numColumns+col]; break;
	}
	return value;
}

/*-----------------------------------------------------------------------**/
int image::getNumberOfRows () 
{
	return data.numRows;
}

/*-----------------------------------------------------------------------**/
int image::getNumberOfColumns () 
{
	return data.numColumns;
}


/*-----------------------------------------------------------------------**/
vector<int>* image::getChannel (int rgb) 
{
    vector<int>* channel;

	switch (rgb) {
        case RED: 
        case GREY:
            channel = &data.redChannel;
            break;
		case GREEN:
            channel = &data.greenChannel;
            break;
        case BLUE:
            channel = &data.blueChannel;
		    break;
        default:
            channel = NULL;
	}
    return channel;
}
	/*-----------------------------------------------------------------------**/
bool image::setChannel (int rgb, vector<int> &channel)
{
	if (channel.size() == this->getChannel(rgb)->size())
	{
		*(this->getChannel(rgb)) = channel;
		return true;
	}
	return false;
}


/*-------------------------------------------------------------------*/
bool image::save (const char* file)
{
	FILE *outfile;
	int flag;

	//fprintf(stderr, "\n Saving edge image in %s...\n", file);
	if ((outfile = fopen(file, "w")) == NULL) 
	{
		printf("Cannot open file, %s for writing.\n", file) ;
		return false;
	}

	if (strstr(file, ".ppm") != NULL) 
	{	/* PPM Color File*/
		flag = 0;
	}

	else flag = 1;

	if (flag)
		fprintf(outfile, "P5\n%d %d\n255\n", data.numColumns, data.numRows);
	else 
		fprintf(outfile, "P6\n%d %d\n255\n", data.numColumns, data.numRows);

	if (flag) 
	{
		for (int i=0; i < data.numRows; i++) 
		{
			for (int j=0; j < data.numColumns; j++)
			{
#if 0 // FIXME: unused
				int v = (data.redChannel [i*data.numColumns+j]>255)?255:data.redChannel [i*data.numColumns+j];
#endif
				putc((unsigned char) (
					((data.redChannel [i*data.numColumns+j])>255)?255:(data.redChannel [i*data.numColumns+j]) ), outfile) ;
			}
		}
	}
	else {
		for (int i=0; i < data.numRows; i++) 
		{
			for (int j=0; j < data.numColumns; j++) 
			{
				putc((unsigned char) (((data.redChannel [i*data.numColumns+j])>255)?255:(data.redChannel [i*data.numColumns+j])), outfile) ;
				putc((unsigned char) (((data.greenChannel [i*data.numColumns+j])>255)?255:(data.greenChannel [i*data.numColumns+j])), outfile) ;
				putc((unsigned char) (((data.blueChannel [i*data.numColumns+j])>255)?255:(data.blueChannel [i*data.numColumns+j])), outfile) ;
			}
		}
	}

	fclose(outfile);
	return true;
}

/*-------------------------------------------------------------------*/
bool image::save (char* file)
{
	const char* name = file;
	return save(name);
}
	/*-------------------------------------------------------------------*/

bool image::read (const char * file) 
{
	FILE *fp;
	char str[50];
	unsigned char r, g, b;
	int i, flag;

	if (strstr(file, ".ppm") != NULL) {/* PPM Color File*/
		flag = 0;
	}
	else flag = 1;

	if ((fp = fopen(file, "r")) == NULL) {
		fprintf(stderr, "\nCannot open image file %s\n", file);
		exit(1);
	}

	fscanf(fp, "%s", str) ;
	if ((flag == 0) && (strcmp (str, "P6") != 0)) {
		fprintf(stderr, "\n image file %s not in PPM format...%s", file, str);
		return false;
	}
	if ((flag == 1) && (strcmp (str, "P5") != 0)) {
		fprintf(stderr, "\n image file %s not in PGM format...%s", file, str);
		return false;
	}

	data.numColumns = getint(fp);
	while (data.numColumns == -1 || data.numColumns == 0)
		data.numColumns = getint(fp);

	while (data.numRows == -1 || data.numRows == 0)
		data.numRows = getint(fp);
	resize (data.numRows,data.numColumns);
	fscanf(fp, "%d", &i) ;  
	(getc(fp));/* gets the carriage return*/

	if (flag)
	{
		for (i=0; i < data.numRows*data.numColumns; i++) 
		{
			r = (unsigned char) (getc(fp));  
			data.redChannel [i] = r;
		}
	}
	else 
	{
		for (i=0; i < data.numRows*data.numColumns; i++) 
		{
			r = (unsigned char) (getc(fp));  
			g = (unsigned char) (getc(fp));  
			b = (unsigned char) (getc(fp));
			data.redChannel [i] = r;
			data.greenChannel [i] = g;
			data.blueChannel [i] = b;
		}
	} 

	fclose(fp);
	return true;
}

/*-----------------------------------------------------------------------**/
int image::getint(FILE *fp) 
{
	int item, i, flag;

/* skip forward to start of next number */
	item  = getc(fp); flag = 1;
	do {

		if (item =='#') {   /* comment*/
			while (item != '\n' && item != EOF) item=getc(fp);
		}

		if (item ==EOF) return 0;
		if (item >='0' && item <='9') 
			{flag = 0;  break;}

	/* illegal values */
		if ( item !='\t' && item !='\r' 
			&& item !='\n' && item !=',') return(-1);

		item = getc(fp);
	} while (flag == 1);


/* get the number*/
	i = 0; flag = 1;
	do {
		i = (i*10) + (item - '0');
		item = getc(fp);
		if (item <'0' || item >'9') {
			flag = 0; break;}
			if (item==EOF) break; 
		} while (flag == 1);

		return i;
	}


/*-----------------------------------------------------------------------**/
bool image::isEmpty() 
{
    return !this->data.numRows;
}

/*-----------------------------------------------------------------------**/

double min(double a, double b, double c)
{
    if (a < b)
    {
        if (a < c)
        {
            return a;
        }
    }
    else
    {
        if (b < c)
        {
            return b;
        }
    }

    return c;
}

void rgb2hsi(int r, int g, int b, double *h, double *s, double *i)
{
    assert(r <= RGB_MAX);
    assert(g <= RGB_MAX);
    assert(b <= RGB_MAX);

    int n;
    double rn,gn,bn;
    double hn,sn,in;
    double t;

    n = r + g + b; 
    rn = (double) r/n;
    gn = (double) g/n;
    bn = (double) b/n;

    t = (rn - gn) * (rn - gn) + (rn - bn) * (gn - bn);
    if (t == 0)
    {
        hn = 0;
    }
    else
    {
        t = 0.5 * ((rn - gn) + (rn - bn)) / sqrt(t);
        if (bn <= gn)
        {
            hn = acos(t);
        }
        else
        {
            hn = 2 * PI - acos(t);
        }
    }

    sn = 1 - 3 * min(rn, gn, bn);
    in = ((double) r + g + b) / (NCHANNELS * RGB_MAX);

    // Convert to integral ranges for output
    *h = (hn * 180 / PI);
    *s = (sn * 100);
    *i = (in * 255);

    // H, S, I must be within their respective ranges
    if (*h > H_MAX)
    {
        *h = H_MAX;
    }
    if (*s > S_MAX)
    {
        *s = S_MAX;
    }
    if (*i > I_MAX)
    {
        *i = I_MAX;
    }
    if (*h < 0)
    {
        *h = 0;
    }
    if (*s < 0)
    {
        *s = 0;
    }
    if (*i < 0)
    {
        *i = 0;
    }
}

void hsi2rgb(double h, double s, double i, int *r, int *g, int *b)
{
    assert(h <= H_MAX);
    assert(s <= S_MAX);
    assert(i <= I_MAX);

    double h_, s_, i_;
    double x=0, y=0, z=0;
    double *pr; 
    double *pg;
    double *pb;

    // Convert to normalized ranges
    h_ = (double) h * PI / 180;
    s_ = (double) s / 100;
    i_ = (double) i / 255;

    if (h_ >= 2 * PI)
    {
        h_ = 0;
    }

    if (h_ < 2 * PI / 3)
    {
        pr = &y;
        pg = &z;
        pb = &x;
    }
    else if (2 * PI / 3 <= h_  && h_ < 4 * PI / 3)
    {
        h_ = h_ - (2 * PI / 3);
        pr = &x;
        pg = &y;
        pb = &z;
    }
    else if (4 * PI / 3 <= h_ && h_ < 2 * PI)
    {
        h_ = h_ - (4 * PI / 3);
        pr = &z;
        pg = &x;
        pb = &y;
    }

    x = i_ * (1 - s_);
    y = i_ * (1 + (s_ * cos(h_)) / cos(PI / 3 - h_));
    z = 3 * i_ - (x + y);

    // Normalized r,g,b values must be [0,1]
    if (*pr > 1)
    {
        *pr = 1;
    }
    if (*pg > 1)
    {
        *pg = 1;
    }
    if (*pb > 1)
    {
        *pb = 1;
    }

    *r = (int) round(*pr * RGB_MAX);
    *g = (int) round(*pg * RGB_MAX);
    *b = (int) round(*pb * RGB_MAX);
}

void ImageHSI::resize(int nRows, int nColumns)
{
    _numRows = nRows;
    _numColumns = nColumns;

    int n = _numRows * _numColumns;
    _h.assign(n, 0);
    _s.assign(n, 0);
    _i.assign(n, 0);
}

//
// Generate HSI values from an RGB image
//
void ImageHSI::_genHSI(image &img)
{
    ImageHSI::resize(img.getNumberOfRows(), img.getNumberOfColumns());
    
    // Generate HSI values
    int r,g,b;
    double h,s,i;
    for (int row = 0; row < getNumberOfRows(); row++)
    {
        for (int col = 0; col < getNumberOfColumns(); col++)
        {
            r = img.getPixel(row, col, RED);
            g = img.getPixel(row, col, GREEN);
            b = img.getPixel(row, col, BLUE);

            rgb2hsi(r, g, b, &h, &s, &i);

            setPixel(row, col, HUE, (int) h);
            setPixel(row, col, SATURATION, (int) s);
            setPixel(row, col, INTENSITY, (int) i);
        }
    }
}

ImageHSI::ImageHSI(image &img)
{
    _genHSI(img);
}

int ImageHSI::getPixel(int row, int col, int channel)
{
    int value;
    int i = row * getNumberOfColumns() + col;

    switch (channel)
    {
        case HUE: 
            value = _h[i]; 
            break;
        case SATURATION: 
            value = _s[i];
            break;
        case INTENSITY: 
            value = _i[i];
            break;
        default:
            throw IPException("Invalid HSI channel value");
    }

    return value;
}

void ImageHSI::setPixel(int row, int col, int channel, int value)
{
    int i = row * getNumberOfColumns() + col;

    switch (channel)
    {
        case HUE: 
            _h[i] = value;
            break;
        case SATURATION: 
            _s[i] = value;
            break;
        case INTENSITY: 
            _i[i] = value;
            break;
        default:
            throw IPException("Invalid HSI channel value");
    }
}

void ImageHSI::toRGB(image &img)
{
    // Convert HSI channels back to RGB
    int h,s,i,r,g,b;
    for (int row = 0; row < getNumberOfRows(); row++)
    {
        for (int col = 0; col < getNumberOfColumns(); col++)
        {
            h = getPixel(row, col, HUE);
            s = getPixel(row, col, SATURATION);
            i = getPixel(row, col, INTENSITY);

            hsi2rgb(h, s, i, &r, &g, &b);

            img.setPixel(row, col, RED, r);
            img.setPixel(row, col, GREEN, g);
            img.setPixel(row, col, BLUE, b);
        }
    }
}

void ImageHSI::toRGB(image &img, int x, int y, int sx, int sy)
{
    // Convert HSI channels back to RGB
    int h,s,i,r,g,b;
    for (int row = y; row < y + sy; row++)
    {
        for (int col = x; col < x + sx; col++)
        {
            h = getPixel(row, col, HUE);
            s = getPixel(row, col, SATURATION);
            i = getPixel(row, col, INTENSITY);

            hsi2rgb(h, s, i, &r, &g, &b);

            img.setPixel(row, col, RED, r);
            img.setPixel(row, col, GREEN, g);
            img.setPixel(row, col, BLUE, b);
        }
    }
}

vector<int>* ImageHSI::getChannel(int channel)
{
    vector<int> *pch;

    switch (channel)
    {
        case HUE: 
            pch = &_h;
            break;
        case SATURATION: 
            pch = &_s;
            break;
        case INTENSITY: 
            pch = &_i;
            break;
        default:
            throw IPException("Invalid HSI channel value");
    }

    return pch;
}


