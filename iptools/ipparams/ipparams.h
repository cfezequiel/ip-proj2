// ipparams.h: Head file for IPParams class
#ifndef IPPARAMS_H
#define IPPARAMS_H

#include <string>
#include <vector>
#include "../image/image.h"
#include "../video/video.h"

class ROI
{
public:
    int x;
    int y;
    int sx;
    int sy;

    bool inImage(image &img);
};

class SOI
{
public:
    int beg;
    int end;

    bool inSOI(frame &frm);
};
        
class IPParam
{
public:
    ROI roi;
    SOI soi;
    std::string filter;
    std::vector<int> filter_params;
};

vector<IPParam> loadIPParams(char *filename);

#endif // IPPARAMS_H
