// ipparams.c: Read image processing parameters from file

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <ctype.h>
#include "../common.h"
#include "../image/image.h"
#include "../video/video.h"
#include "ipparams.h"
using namespace std;

/**
 * Checks if the ROI is within a given image
 */
bool ROI::inImage(image &img)
{
    int xmax;
    int ymax;
    int imgWidth = img.getNumberOfColumns();
    int imgHeight = img.getNumberOfRows();

    xmax = this->x + this->sx;
    ymax = this->y + this->sy;

    if (this->x < 0 || this->y < 0 || xmax > imgWidth || ymax > imgHeight)
    {
        return false;
    }

    return true;
}

bool SOI::inSOI(frame &frm)
{
    if (frm.seqNum >= this->beg && frm.seqNum <= this->end)
    {
        return true;
    }

    return false;
}


vector<IPParam> loadIPParams(char *filename)
{
    ifstream file;
    int nParams;
    vector<IPParam> params;

    // Open file
    file.open(filename, ifstream::in);

    // Get number of parameters
    file >> nParams;

    // Read each parameter line
    for (int i = 0; i < nParams; i++)
    {
        IPParam param;

        // Read ROI
        file >> param.roi.x;
        file >> param.roi.y;
        file >> param.roi.sx;
        file >> param.roi.sy;

        // Read SOI
        file >> param.soi.beg;
        file >> param.soi.end;

        // Read filter type
        file >> param.filter;

        // Read filter parameters
        int val;
        string line;
        getline(file, line);
        istringstream ss(line);
        while (ss.good())
        {
            ss >> val;
            param.filter_params.push_back(val);
        }

        params.push_back(param);
    }

    file.close();

    return params;
}
