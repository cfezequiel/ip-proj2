#! /usr/bin/python
# Test generator

import copy
import os


PROGRAM="../bin/iptool"

class SOI:
    """Sequence of interest parameter."""

    def __init__(self, beg, end):
        self.beg = beg
        self.end = end

class ROI:
    """ Region of interest parameter."""

    def __init__(self, x, y, sx, sy):
        self.x = x
        self.y = y
        self.sx = sx
        self.sy = sy

class IPFilter:

    def __init__(self, name, params):
        self.name = name
        self.params = params

    def __str__(self):
        return self.name

class IPFilterHE(IPFilter):
    """Histogram equalization filter."""

    c_colorspace = {'grey' : 0,
                   'rgb' : 1,
                   'hsi' : 2}
    c_channels = {'r': 1,
                  'g': 2,
                  'b': 4,
                  'h': 1,
                  's': 2,
                  'i': 4}


    def __init__(self, colorspace, channels=[]):
        self.name = "histoequalize"

        self.params = [self.c_colorspace[colorspace]]
        if colorspace != 'grey':
            param = 0
            for ch in channels:
                param = param | self.c_channels[ch]
            self.params.append(param)

class IPParam:
    """Represents parameters for one test."""

    def __init__(self, f, roi, soi=SOI(1,1)):
        self.f = f
        self.roi = roi
        self.soi = soi

class IPParamFile:
    """Represents a parameter file."""
    
    def __init__(self, filename, ipparams=[]):
        self.ipparams = ipparams
        self.filename = filename

    def save(self):
        fp = open(self.filename, "w")
        fp.write(str(len(self.ipparams)) + "\n")
        for p in self.ipparams:
            fp.write("%s %s %s %s " % (p.roi.x, p.roi.y, p.roi.sx, p.roi.sy))
            fp.write("%s %s " % (p.soi.beg, p.soi.end))
            fp.write("%s " % p.f)
            for filter_params in p.f.params:
                fp.write("%s " % filter_params)
            fp.write("\n")
        fp.close()

    def __str__(self):
        return self.filename

class IPTest:
    """ Represents an iptool test."""

    def __init__(self, src, tgt, p, opts=''):
        self.src = src 
        self.tgt = tgt
        self.p = p
        self.opts =opts

    def cmdstr(self):
        """Return command line string."""

        # Get performance
        s = "echo " + self.__str__() + '\n'
        s += "time %s %s %s %s %s" % (PROGRAM, self.opts, self.src, self.tgt, self.p)
        return s

    def run(self):
        """Run this test."""

        self.p.save()
        cmd = self.cmdstr()
        status = os.system(cmd)
        if (status):
            print "FAILED! -> %s" % cmd

    def save(self):
        """Save parameter file."""
        self.p.save()

    def __str__(self):
        s = "IPTest: src=%s, tgt=%s, param=%s" % (self.src, self.tgt, self.p)
        return s

class IPTestHE:
    """ Represents a set of tests for histogram equalization."""

    def __init__(self, src, tgt, p, opts=''):
        
        self.src = src
        self.tgt = tgt
        self.p = p
        self.tests = []

        # Special case for '-s' option
        if opts.find("-s") != -1:
            if opts.find("-g") != 1:
                extH = ".pgm"
            else:
                extH = ".ppm"

            # Create test to generate histogram (pre-HE)
            (path, ext) = os.path.splitext(tgt)
            tgt_pre = path + "_histo_pre" + extH
            ph = copy.deepcopy(p)
            (path, ext) = os.path.splitext(ph.filename)
            ph.filename = path + "_histo_pre" + ext
            for ipparam in ph.ipparams:
                ipparam.f.name = 'histogram'
            self.tests.append(IPTest(src, tgt_pre, ph, opts))

            # Create test for histogram equalization
            self.tests.append(IPTest(src, tgt, p, opts))

            # Create test to generate histogram (post-HE)
            (path, ext) = os.path.splitext(tgt)
            tgt_post = path + "_histo" + extH
            self.tests.append(IPTest(tgt, tgt_post, ph, opts))

        else:
            # Create test to generate histogram (pre-HE)
            (path, ext) = os.path.splitext(tgt)
            tgt_pre = path + "_histo_pre" + ext
            ph = copy.deepcopy(p)
            (path, ext) = os.path.splitext(ph.filename)
            ph.filename = path + "_histo_pre" + ext
            for ipparam in ph.ipparams:
                ipparam.f.name = 'histogram'
            self.tests.append(IPTest(src, tgt_pre, ph, opts))

            # Create test for histogram equalization
            self.tests.append(IPTest(src, tgt, p, opts))

            # Create test to generate histogram (post-HE)
            (path, ext) = os.path.splitext(tgt)
            tgt_post = path + "_histo" + ext
            self.tests.append(IPTest(tgt, tgt_post, ph, opts))

    def cmdstr(self):

        s = ""
        for test in self.tests:
            s += test.cmdstr() + '\n'
        return s

    def run(self):
        for test in self.tests:
            test.run()

    def save(self):
        for test in self.tests:
            test.save()

class IPTestRunner:
    """Executes a set of IP tests."""

    def __init__(self, tests=[]):
        self.tests = tests

    def run(self):
        """Run all tests."""

    def add(self, test):
        self.tests.append(test)

    def clear(self):
        """Remove all tests."""
        self.tests = []

    def save(self, filename):
        """Save the tests to given file."""

        fp = open(filename, "w")
        for test in self.tests:
            test.save()
            fp.write(test.cmdstr()+ '\n')
        fp.close()

        os.system('chmod 744 %s' % filename)


if __name__ == '__main__':
    """ THE tests. """

    IMG_GREY='input/microscopy_grey_stack1_Z27.pgm'
    # Size: 800x600

    IMG_COLOR='input/baboon_color.ppm'
    # Size: 512x512

    IMG_COLOR2='input/microscopy_color_stack1_Z27.ppm'
    # Size: 800x600

    IMG_COLOR3='input/microscopy_color_stack1_Z36_large.ppm'
    # Size: 1096x822

    VID_GREY='input/microscopy_grey_stack3.m1v'
    # Size: 800x600
    # Fps: 25
    # Frames: 20

    VID_COLOR='input/gangnam_style_clip.m1v'
    # Size: 768x576
    # Fps: 25
    # Frames: 56

    tr= IPTestRunner()

    # Image tests 

    # Greyscale tests

    tr.add(IPTestHE(IMG_GREY, 'output/he_grey.pgm', 
                    IPParamFile('conf/he_grey.p',
                                [IPParam(IPFilterHE('grey'), ROI(72, 322, 256, 256)),
                                 IPParam(IPFilterHE('grey'), ROI(372, 22, 256, 256))])))

    # RGB tests                               
    cs = 'rgb'                                
    ch = 'r'
    tr.add(IPTestHE(IMG_COLOR, 'output/he_%s_%s.ppm' % (cs, ch), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, ch),
                                [IPParam(IPFilterHE(cs, [ch]), ROI(75, 75, 256, 256))])))

    ch = 'g'
    tr.add(IPTestHE(IMG_COLOR, 'output/he_%s_%s.ppm' % (cs, ch), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, ch),
                                [IPParam(IPFilterHE(cs, [ch]), ROI(75, 75, 256, 256))])))

    ch = 'b'
    tr.add(IPTestHE(IMG_COLOR, 'output/he_%s_%s.ppm' % (cs, ch), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, ch),
                                [IPParam(IPFilterHE(cs, [ch]), ROI(75, 75, 256, 256))])))

    tr.add(IPTestHE(IMG_COLOR2, 'output/he_%s_%s.ppm' % (cs, 'sep'), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, 'sep'),
                                [IPParam(IPFilterHE(cs, ['r']), ROI(472, 22, 256, 256)),
                                 IPParam(IPFilterHE(cs, ['g']), ROI(72, 322, 256, 256)),
                                 IPParam(IPFilterHE(cs, ['b']), ROI(472, 322, 256, 256))\
                                ])))

    tr.add(IPTestHE(IMG_COLOR2, 'output/he_%s_%s.ppm' % (cs, 'comb'), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, 'comb'),
                                [IPParam(IPFilterHE(cs, ['r', 'g', 'b']), ROI(72, 322, 256, 256)),
                                 IPParam(IPFilterHE(cs, ['r', 'g', 'b']), ROI(372, 22, 256, 256))\
                                ])))

    # HSI tests
    cs = 'hsi'
    ch = 'h'
    tr.add(IPTestHE(IMG_COLOR, 'output/he_%s_%s.ppm' % (cs, ch), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, ch),
                                [IPParam(IPFilterHE(cs, [ch]), ROI(75, 75, 361, 361))])))

    ch = 's'
    tr.add(IPTestHE(IMG_COLOR, 'output/he_%s_%s.ppm' % (cs, ch), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, ch),
                                [IPParam(IPFilterHE(cs, [ch]), ROI(75, 75, 361, 361))])))

    ch = 'i'
    tr.add(IPTestHE(IMG_COLOR, 'output/he_%s_%s.ppm' % (cs, ch), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, ch),
                                [IPParam(IPFilterHE(cs, [ch]), ROI(75, 75, 361, 361))])))

    tr.add(IPTestHE(IMG_COLOR3, 'output/he_%s_%s.ppm' % (cs, 'sep'), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, 'sep'),
                                [IPParam(IPFilterHE(cs, ['h']), ROI(436, 25, 361, 361)),
                                 IPParam(IPFilterHE(cs, ['s']), ROI(25, 436, 361, 361)),
                                 IPParam(IPFilterHE(cs, ['i']), ROI(436, 436, 361, 361))\
                                ])))

    tr.add(IPTestHE(IMG_COLOR3, 'output/he_%s_%s.ppm' % (cs, 'comb'), 
                    IPParamFile('conf/he_%s_%s.p' % (cs, 'comb'),
                                [IPParam(IPFilterHE(cs, ['h', 's', 'i']), ROI(436, 25, 361, 361)),
                                 IPParam(IPFilterHE(cs, ['h', 's', 'i']), ROI(25, 436, 361, 361))\
                                ])))

    tr.save("run_img.sh")

    # Video tests
    tr.clear()

    # Grey tests (HE per frame)
    tr.add(IPTestHE(VID_GREY, 'output/vid_he_grey_frame.m1v', 
                    IPParamFile('conf/vid_he_grey_frame.p',
                                [IPParam(IPFilterHE('grey'), ROI(272, 22, 256, 256), SOI(1, 10)),
                                 IPParam(IPFilterHE('grey'), ROI(272, 322, 256, 256), SOI(5, 15))\
                                ]), '-g'))

    # Grey tests (HE per SOI)
    tr.add(IPTestHE(VID_GREY, 'output/vid_he_grey_soi.m1v', 
                    IPParamFile('conf/vid_he_grey_soi.p',
                                [IPParam(IPFilterHE('grey'), ROI(272, 22, 256, 256), SOI(6, 10)),
                                 IPParam(IPFilterHE('grey'), ROI(272, 322, 256, 256), SOI(5, 15))\
                                ]), '-g -s'))

    # RGB tests (HE per frame)
    cs = 'rgb'
    '''
    tr.add(IPTestHE(VID_COLOR, 'output/vid_he_%s_%s_frame.m1v' % (cs, 'sep'), 
                    IPParamFile('conf/vid_he_%s_%s_frame.p' % (cs, 'sep'),
                                [IPParam(IPFilterHE(cs, ['r']), ROI(203, 107, 361, 361), SOI(1, 10)),
                                 IPParam(IPFilterHE(cs, ['g']), ROI(203, 107, 361, 361), SOI(16, 30)),
                                 IPParam(IPFilterHE(cs, ['b']), ROI(203, 107, 361, 361), SOI(31, 45)),
                                ])))
    '''

    tr.add(IPTestHE(VID_COLOR, 'output/vid_he_%s_%s_frame.m1v' % (cs, 'comb'), 
                    IPParamFile('conf/vid_he_%s_%s_frame.p' % (cs, 'comb'),
                                [IPParam(IPFilterHE(cs, ['r', 'g', 'b']), ROI(203, 107, 361, 361), SOI(1, 40))\
                                ])))
    # RGB tests (HE per SOI)
    '''
    tr.add(IPTestHE(VID_COLOR, 'output/vid_he_%s_%s_soi.m1v' % (cs, 'sep'), 
                    IPParamFile('conf/vid_he_%s_%s_soi.p' % (cs, 'sep'),
                                [IPParam(IPFilterHE(cs, ['r']), ROI(203, 107, 361, 361), SOI(1, 10)),
                                 IPParam(IPFilterHE(cs, ['g']), ROI(203, 107, 361, 361), SOI(16, 30)),
                                 IPParam(IPFilterHE(cs, ['b']), ROI(203, 107, 361, 361), SOI(31, 45)),
                                ]), '-s'))

    tr.add(IPTestHE(VID_COLOR, 'output/vid_he_%s_%s_soi.m1v' % (cs, 'comb'), 
                    IPParamFile('conf/vid_he_%s_%s_soi.p' % (cs, 'comb'),
                                [IPParam(IPFilterHE(cs, ['r', 'g', 'b']), ROI(203, 107, 361, 361), SOI(1, 40))\
                                ]), '-s'))
    '''


    # HSI tests (HE per frame)
    cs = 'hsi'
    '''
    tr.add(IPTestHE(VID_COLOR, 'output/vid_he_%s_%s_frame.m1v' % (cs, 'sep'), 
                    IPParamFile('conf/vid_he_%s_%s_frame.p' % (cs, 'sep'),
                                [IPParam(IPFilterHE(cs, ['h']), ROI(203, 107, 361, 361), SOI(1, 10)),
                                 IPParam(IPFilterHE(cs, ['s']), ROI(203, 107, 361, 361), SOI(16, 30)),
                                 IPParam(IPFilterHE(cs, ['i']), ROI(203, 107, 361, 361), SOI(31, 45)),
                                ])))
    '''

    tr.add(IPTestHE(VID_COLOR, 'output/vid_he_%s_%s_frame.m1v' % (cs, 'comb'), 
                    IPParamFile('conf/vid_he_%s_%s_frame.p' % (cs, 'comb'),
                                [IPParam(IPFilterHE(cs, ['h', 's', 'i']), ROI(203, 107, 361, 361), SOI(1, 40))\
                                ])))


    # HSI tests (HE per SOI)
    '''
    tr.add(IPTestHE(VID_COLOR, 'output/vid_he_%s_%s_soi.m1v' % (cs, 'sep'), 
                    IPParamFile('conf/vid_he_%s_%s_soi.p' % (cs, 'sep'),
                                [IPParam(IPFilterHE(cs, ['h']), ROI(203, 107, 361, 361), SOI(1, 10)),
                                 IPParam(IPFilterHE(cs, ['s']), ROI(203, 107, 361, 361), SOI(16, 30)),
                                 IPParam(IPFilterHE(cs, ['i']), ROI(203, 107, 361, 361), SOI(31, 45)),
                                ]), '-s'))

    # Special case (histogram images, histogram equalized video)
    tr.add(IPTestHE(VID_COLOR, 'output/vid_he_%s_%s_soi.m1v' % (cs, 'comb'), 
                    IPParamFile('conf/vid_he_%s_%s_soi.p' % (cs, 'comb'),
                                [IPParam(IPFilterHE(cs, ['h']), ROI(203, 107, 361, 361), SOI(1, 10)),
                                 IPParam(IPFilterHE(cs, ['s']), ROI(203, 107, 361, 361), SOI(16, 30)),
                                 IPParam(IPFilterHE(cs, ['i']), ROI(203, 107, 361, 361), SOI(31, 45)),
                                ]), '-s'))
    '''

    tr.save("run_vid.sh")
