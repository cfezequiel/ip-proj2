#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <strings.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fstream>
#include <string>
#include <string.h>
#include <vector>
#include <algorithm>
#include "../iptools/core.h"
using namespace std;

#define MAXLEN 256

string getext(string path)
{
    int dotPos = path.find_last_of(".");

    // Get extension without the "."
    string ext = path.substr(dotPos + 1);

    return ext;
}


int main (int argc, char** argv)
{
    // Check parameters
    if (argc < 4)
    {
        cout << "Syntax: " << argv[0] << " <SRC> <DST> <parameter file> [-g]"
             << endl;
        return -1;
    }

    COLORSPACE cs = CS_RGB;
    VID_MODE mode = PER_FRAME;
    int c;
    while ((c = getopt(argc, argv, "gs")) != -1)
    {
        switch (c)
        { 
            case 'g': // Assume greyscale processing
                cs = CS_GREY;
                break;
            case 's':
                mode = PER_SOI;
                break;
            default:
                abort();
        }
    }

    // Read ROI file
    vector<IPParam> params = loadIPParams(argv[optind + 2]);

    // Check if SRC is image or video
    string srcPath(argv[optind]);
    string ext = getext(srcPath);
    string tgtPath(argv[optind + 1]);
    if (ext == "ppm" || ext == "pgm")
    {
        // ----- Process image ----- 

        // Read source image
        image src;
        src.read(srcPath.c_str());

        // Create target image
        image tgt;

        // Apply image filter for each ROI
        vector<IPParam>::iterator iparam;
        for (iparam = params.begin(); iparam != params.end(); iparam++)
        {
            try
            {
                utility::processImage(src, tgt, *iparam);
            }
            catch (IPException& e)
            {
                cerr << "Error in processing image: " << e.what() << endl;
            }
        }

        // Save modified image
        tgt.save(tgtPath.c_str());
    }
    else if (ext == "m1v" || ext == "mpg")
    {
        // ----- Process video -----
        string tgtext = getext(tgtPath);
        if (tgtext == "ppm" || tgtext == "pgm")
        {
            image imgtgt;
            utility::processVideo(srcPath, imgtgt, params, cs, mode);
            imgtgt.save(tgtPath.c_str());
        }
        else
        {
            utility::processVideo(srcPath, tgtPath, params, cs, mode);
        }
    }
    else
    {
        cerr << "Unknown image or video type -> " << ext << endl;
    }

    return 0;
}

